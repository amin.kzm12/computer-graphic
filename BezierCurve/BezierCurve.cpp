#include <iostream>
#include <stdlib.h>
#include <GLUT/GLUT.h>
#include <math.h>

using namespace std;


//Point class for taking the points
class Point {
public:
    float x, y;

    void setxy(float x2, float y2)
    {
        x = x2; y = y2;
    }
        
};

int factorial(int n)
{
    if (n<=1)
        return(1);
    else
        n=n*factorial(n-1);
    return n;
}

float binomial_coff(float n,float k)
{
    float ans;
    ans = factorial(n) / (factorial(k)*factorial(n-k));
    return ans;
}




Point abc[20];
int SCREEN_HEIGHT = 500;
int points = 0;
int clicks = 4;
int focusedPoint;
Point transformationPoint;


void myInit() {
    glClearColor(1.0,1.0,1.0,0.0);
    glColor3f(1.0,0.0,1.0);
    glPointSize(20);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0.0,640.0,0.0,500.0);

}

void drawDot(int x, int y) {
    glBegin(GL_POINTS);
     glVertex2i(x,y);
    glEnd();
    glFlush();
}

void drawLine(Point p1, Point p2) {
    glBegin(GL_LINES);
      glVertex2f(p1.x, p1.y);
      glVertex2f(p2.x, p2.y);
    glEnd();
    glFlush();
}


//Calculate the bezier point
Point drawBezier(Point PT[], double t) {
    
    //\mathbf {B} (t)=(1-t)^{3}\mathbf {P} _{0}+3(1-t)^{2}t\mathbf {P} _{1}+3(1-t)t^{2}\mathbf {P} _{2}+t^{3}\mathbf {P} _{3}{\mbox{ , }}0\leq t\leq 1.
    Point P;
    P.x = pow((1 - t), 3) * PT[0].x + 3 * t * pow((1 -t), 2) * PT[1].x + 3 * (1-t) * pow(t, 2)* PT[2].x + pow (t, 3)* PT[3].x;
    P.y = pow((1 - t), 3) * PT[0].y + 3 * t * pow((1 -t), 2) * PT[1].y + 3 * (1-t) * pow(t, 2)* PT[2].y + pow (t, 3)* PT[3].y;

    return P;
}


//Calculate the bezier point [generalized]


Point drawBezierGeneralized(Point PT[], double t) {
        Point P;
        P.x = 0; P.y = 0;
        for (int i = 0; i<clicks; i++)
        {
            P.x = P.x + binomial_coff((float)(clicks - 1), (float)i) * pow(t, (double)i) * pow((1 - t), (clicks - 1 - i)) * PT[i].x;
            P.y = P.y + binomial_coff((float)(clicks - 1), (float)i) * pow(t, (double)i) * pow((1 - t), (clicks - 1 - i)) * PT[i].y;
        }
        //cout<<P.x<<endl<<P.y;
        //cout<<endl<<endl;
        return P;
    }

void doProcessAndDrawBezierCurve(int x , int y){
    abc[points].setxy((float)x,(float)(SCREEN_HEIGHT - y));
        points++;

       
        drawDot(x, SCREEN_HEIGHT - y);


        
        if(points == clicks)
        {
            glColor3f(1.0,0.0,1.0);
            for(int k=0;k<clicks-1;k++)
                drawLine(abc[k], abc[k+1]);

            Point p1 = abc[0];

            for(double t = 0.0;t <= 1.0; t += 0.02)
            {
                Point p2 = drawBezierGeneralized(abc,t);
    //            cout<<p1.x<<"  ,  "<<p1.y<<endl;
    //            cout<<p2.x<<"  ,  "<<p2.y<<endl;
                cout<<endl;
                glColor3f(0.627 + t,0.658 + t,.196 +t);
                drawLine(p1, p2);
                p1 = p2;
            }
            glColor3f(1.0,0.0,1.0);
            points = 0;
                
        }
}

int findPointInPoints(Point points[] , Point suppose){
    for(int i=0 ; i < 4 ; i++){
        if(points[i].x == suppose.x && points[i].y == suppose.y){
            return i;
        }
    }
    return -1;
}


void printPoints(){
    for(int i=0;i < 4;i++){
        cout<<"x : "<<abc[i].x<<endl;
        cout<<"y : "<<abc[i].y<<endl;
    }
}

void myMouse(int button, int state, int x, int y) {
  // If left button was clicked
  if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
    // Store where mouse was clicked, Y is backwards.
      doProcessAndDrawBezierCurve(x,y);
  }

//  else if(state == GLUT_DOWN){
//       //glutPostRedisplay();
//      switch (focusedPoint) {
//      case 0:
//        abc[0].x = x;
//        abc[0].y = SCREEN_HEIGHT - y;
//        break;
//      case 1:
//        abc[1].x = x;
//        abc[1].y = SCREEN_HEIGHT - y;
//        break;
//      case 2:
//        abc[2].x = x;
//        abc[2].y = SCREEN_HEIGHT - y;
//              abc[2].x = x;
//                abc[2].y = SCREEN_HEIGHT - y;
//              drawDot(abc[3].x, abc[3].y);
//        break;
//      case 3:
//        abc[3].x = x;
//        abc[3].y = SCREEN_HEIGHT - y;
//
//
//        drawDot(abc[3].x, abc[3].y);
//
//        break;
//      }
//
//
//
//  }
}




void myDisplay() {
    glClear(GL_COLOR_BUFFER_BIT);
    glFlush();
}

auto keyboard(unsigned char button, int x, int y) -> void {
  switch (button) {
  case '1':
  case '2':
  case '3':
  case '4':
    focusedPoint = button - '1';
    break;
  case 'q':
  case 'Q':
    glutDestroyWindow(glutGetWindow());
    exit(0);
  }
}

int main(int argc, char *argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE|GLUT_RGB);
    glutInitWindowSize(640,500);
    glutInitWindowPosition(100,150);
    glutCreateWindow("Bezier Curve");
    glutKeyboardFunc(keyboard);
    glutMouseFunc(myMouse);
    glutDisplayFunc(myDisplay);
    myInit();
    glutMainLoop();
   
    return 0;
}
