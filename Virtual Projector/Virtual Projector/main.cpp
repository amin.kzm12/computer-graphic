#include <iostream>
#include <OpenGL/gl3.h>
#include <GLFW/glfw3.h>

#include <fstream>
#include <string> 
#include <sstream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <vector>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"



#include "Shader.h"
#include "Mesh.h"
#include "Model.h"
#include "Camera.h"
#include "MoveModel.h"

#define HEIGHT 1080
#define WIDTH 1920



void framebufferSizeCallback(GLFWwindow* window, int width, int height);//callback for creating window
void processInput(GLFWwindow*, Camera*, float);//key handler
void renderOjects(Shader, Camera*, Model);
void renderModel(Shader, Camera*, Model, float);

using namespace std;
using namespace glm;


int main() {
    if (!glfwInit()) {
      std::cerr << "GL initialization failed" << std::endl;
      return 1;
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "LearnOpenGL", NULL, NULL);

    if (window == NULL) {
        cout << "Failed to create GLFW window" << endl;
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);


    glEnable(GL_DEPTH_TEST);

    Shader modelShader("/Users/aminkazemi/Desktop/Graphic/hw3/hw4/model_vertexshader.vert","/Users/aminkazemi/Desktop/Graphic/hw3/hw4/model_fragmentshader.frag");
    Model model("/Users/aminkazemi/Desktop/graphic/hw3/hw4/model/nanosuit.obj");
    MoveModel movement;

    glViewport(0, 0, WIDTH, HEIGHT);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);//This function sets the framebuffer resize callback of the specified window


    modelShader.use();
    modelShader.setInt("diffuseTexture", 0);
    modelShader.setInt("shadowMap", 1);


    Camera camera;

    float deltaTime = 0.0f;
    float lastFrame = 0.0f;


    while (!glfwWindowShouldClose(window))
    {
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // input
        // -----
        processInput(window, &camera, deltaTime);

        // render
        // ------
        glClearColor(0.166f, 0.251f, 0.068f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        modelShader.use();
        renderModel(modelShader,&camera,model,movement.move());


        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    exit(1);
    return 0;

}

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

//organzie input valuse
void processInput(GLFWwindow* window, Camera* camera, float deltaTime) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
    camera->processMovementInput(window, deltaTime);
}

void renderModel(Shader sh, Camera* camera, Model mModel, float pos) {
    mat4 projection = mat4(1.0f);
    projection = perspective(radians(45.0f), (float)WIDTH / (float)HEIGHT, 0.1f, 100.0f);
    //It creates a projection matrix, i.e. the matrix that describes the set of linear equations that transforms vectors from eye space into clip space.
    
    //that matrices can be used to describe the relative alignment of a coordinate system within another coordinate system. What the perspective transform does is, that it let's the vertices z-values "slip" into their projected w-values as well. And by the perspective divide a non-unity w will cause "distortion" of the vertex coordinates. Vertices with small z will be divided by a small w, thus their coordinates "blow" up, whereas vertices with large z will be "squeezed", which is what's causing the perspective effect.
    sh.setMVP("projection", projection);

    glm::mat4 view = camera->getViewPosition();//fix our sight
    sh.setMVP("view", view);

    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, glm::vec3(pos, -0.4f, -1.5f)); // translate it down so it's at the center of the scene
    model = glm::rotate(model, glm::radians(-25.0f), glm::vec3(0.0f, -0.2f, 0.0f));//for 3D uage
    model = glm::scale(model, glm::vec3(0.03f, 0.03f, 0.03f));    // it's a bit too big for our scene, so scale it down
    sh.setMVP("model", model);

    mModel.Draw(sh);
}
