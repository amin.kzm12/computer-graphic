#version 410 core
out vec4 FragColor;

in vec2 TexCoords;

in vec3 aNormal;
uniform sampler2D texture_diffuse1;

void main()
{    
    FragColor = vec4(aNormal,1);
}
