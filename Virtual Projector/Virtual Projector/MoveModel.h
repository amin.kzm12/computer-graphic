#pragma once


enum direction { RIGHTDIR, LEFTDIR };

class MoveModel {
private:
	int dir = RIGHTDIR;
	float modelPos = -0.5;
	float step = 0.0015;
	const float maxLeft = -0.5;
	const float maxRight = 0.5;
public:
	float move();
};

float MoveModel::move() {
	if (dir == RIGHTDIR) {
		if (modelPos < maxRight)
			modelPos += step;
		else {
			dir = LEFTDIR;
		}
	}
	if (dir == LEFTDIR) {
		if (modelPos > maxLeft)
			modelPos -= step;
		else {
			dir = RIGHTDIR;
		}
	}
	return modelPos;
}