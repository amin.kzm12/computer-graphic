#pragma once
#include <OpenGL/gl3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;
//llink fragment vertex and compile to show that where to alocate each object(color , texture,coordinate)
class Shader {
public:
	unsigned int shaderProgramID;

	// constructor reads and builds the shader
	Shader(const GLchar* vertexPath, const GLchar* fragmentPath);
	void use();

	void setBool(const string& name, bool value) const;
	void setInt(const string& name, int value) const;
	void setFloat(const string& name, float value) const;
	void setMVP(string matrixName, glm::mat4 projectoin);
	void setVec3(string name, float x, float y, float z);
	glm::mat4 getMVP(string matrixName);
private:
	string readShaderCode(string fileName);
};

Shader::Shader(const char* vertexShaderCode, const char* fragmentShaderCode) {
	//vertexshader
	int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	string str = readShaderCode(vertexShaderCode);
	const char* vertexShaderSource = &str[0];
	glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
	glCompileShader(vertexShader);
	// check for shader compile errors
	int success;
	char infoLog[512];
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
	if (!success) {
		glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	//fragment shader
	int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	str.clear();
	str = readShaderCode(fragmentShaderCode);
	const char* fragmentShaderSource = &str[0];
	glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);
	// check for shader compile errors
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
	if (!success)
	{
		glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
	}
	// link shaders
	shaderProgramID = glCreateProgram();
	glAttachShader(shaderProgramID, vertexShader);
	glAttachShader(shaderProgramID, fragmentShader);
	glLinkProgram(shaderProgramID);
	// check for linking errors
	glGetProgramiv(shaderProgramID, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(shaderProgramID, 512, NULL, infoLog);
		std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
	}
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);
}

void Shader::use() {
	glUseProgram(shaderProgramID);
}

void Shader::setBool(const string& name, bool value)const {
	glUniform1i(glGetUniformLocation(shaderProgramID, name.c_str()), (int)value);
}

void Shader::setInt(const string& name, int value)const {
	glUniform1i(glGetUniformLocation(shaderProgramID, name.c_str()), value);
}

void Shader::setFloat(const string& name, float value)const {
	glUniform1f(glGetUniformLocation(shaderProgramID, name.c_str()), value);
}

void Shader::setMVP(string matrixName, glm::mat4 projection) {
	unsigned int projLoc = glGetUniformLocation(shaderProgramID, matrixName.c_str());
    // returns an integer that represents the location of a specific uniform variable within a program object
	glUniformMatrix4fv(projLoc, 1, GL_FALSE, &projection[0][0]);
    //specifies the number of matrices that are to be modified.
}

void Shader::setVec3(string name, float x, float y, float z) {
	glUniform3f(glGetUniformLocation(shaderProgramID, name.c_str()), x, y, z);
}


string Shader::readShaderCode(string fileName) {
	ifstream readCode(fileName);
	ostringstream ss;
	if (readCode) {
		ss << readCode.rdbuf(); // reading data
	}
	return ss.str();
}
