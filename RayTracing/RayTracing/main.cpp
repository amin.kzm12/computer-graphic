#include <iostream>
#include "glm/glm.hpp"
#include "lodepng.cpp"
#include <string>
#include <vector>



using namespace std;
using  namespace glm;



class LoadImage {
private:
    string fileName;
    int width;
    int height;

public:
    unsigned char* buffer;
    LoadImage(int width , int height){
        this->width = width;
        this->height = height;
        buffer = new unsigned char[width * height * 4];
    }
    LoadImage(string fName , int width , int height){
        this->fileName = fName;
        this->width = width;
        this->height = height;
        buffer = new unsigned char[width * height * 4];
    }
    int getWidth(){
        return this->width;
    }
    int getHeight(){
        return this->height;
    }

    void createImageFile(unsigned char *image) {
        vector<unsigned char>png;
        unsigned error = lodepng::encode(png,image,this->width,this->height);
        if (!error)
            lodepng::save_file(png,this->fileName);
            cout << "Image creation has ended!"<<endl<<"press any key to close the program." << endl;
    }
    void setPixel(int i, int j, vec3 color , float angle){
        
        unsigned char r = static_cast<unsigned char>(color.r);
        unsigned char g = static_cast<unsigned char>(color.g);
        unsigned char b = static_cast<unsigned char>(color.b);

        buffer[i*width*4 + 4*j + 0] = r * angle  ;
        buffer[i*width*4 + 4*j + 1] = g * angle ;
        buffer[i*width*4 + 4*j + 2] = b * angle  ;
        buffer[i*width*4 + 4*j + 3] = 255;
    }
};

class Ray{
private:
    vec3 origin;
    vec3 direction;
public:
    Ray( vec3 origin ,  vec3 direction ){
        this->origin = origin;
        this->direction = direction;
    }
    vec3 getOrigin(){
        return this->origin;
    }
    
    vec3 getDirection(){
        return this->direction;
    }
    vec3 point_at_parameter(float t){
        return this->getOrigin() + (this->getDirection() * t);
        // p is a 3D position along
        //A is the ray origin and B is the ray direction
    }
    

};
class Hitable{
public:
    virtual bool intersect_ray(Ray& ray, vec3* out_intersect_position, vec3* out_intersect_normal, float* out_t,vec3* out_color)=0;
};


class Plane: public Hitable{
public:
    vec3 normal;
    float c;
    vec3 color;
    Plane(vec3 normal,float c,vec3 color):normal(normal),c(c),color(color){}
    bool intersect_ray(Ray& ray, vec3* out_intersect_position, vec3* out_intersect_normal, float* out_t,vec3* out_color){
        float denom = dot(normal,ray.getDirection());
        if(abs(denom)<0.00001f) return false;

        float t = (c - dot(normal,ray.getOrigin()))/denom;
        if(t<0) return false;

        if(out_intersect_position != nullptr){
            *out_intersect_position = ray.point_at_parameter(t);
        }
        if(out_t!=nullptr){
            *out_t = t;
        }
        if(out_intersect_normal != nullptr){
            *out_intersect_normal = normal;
        }
        if(out_color!= nullptr){
            *out_color = color;
        }
        return true;

    }
};
class Camera{
public:
    LoadImage image;
    float fov;
    float world_space_min_y;
    float world_space_height;

    float world_space_min_x;
    float world_space_width;
    Camera(string fileName,int width,int height,float fov):image(fileName,width,height),fov(fov){
        world_space_min_y = -tanf(fov/2);
        world_space_height = 2*tanf(fov/2);
        float aspect_ratio = static_cast<float>(image.getWidth())/image.getHeight();
        world_space_min_x = aspect_ratio *world_space_min_y;
        world_space_width = aspect_ratio*world_space_height;
    }

    Ray get_pixel_ray(int i,int j){
        float x_ratio = static_cast<float>(i)/image.getWidth();
        float y_ratio = static_cast<float>(j)/image.getHeight();
        float worldspace_x = world_space_min_x + x_ratio*world_space_width;
        float worldspace_y = world_space_min_y + y_ratio*world_space_height;

        return Ray(vec3(0,0,0),normalize(vec3(worldspace_x,worldspace_y,-1)));
    }

};

bool solve_quadratic_quation(float a, float b, float c, float* s1, float* s2){
    float delta = b*b - 4*a*c;
    if (delta < 0) return false;
    *s1 = (-b + sqrt(delta))/2*a;
    *s2 = (-b - sqrt(delta))/2*a;
    return true;
}

class Sphere : public Hitable{
    public:
    float r;
    vec3 center;
    vec3 color;
    Sphere(vec3 center, float r, vec3 color) : center(center), r(r), color(color){}

    bool intersect_ray(Ray& ray, vec3* out_intersect_position, vec3* out_intersect_normal, float* out_t, vec3* out_color){
        float a = dot(ray.getDirection(), ray.getDirection());
        float b = 2*dot(ray.getOrigin()-center, ray.getDirection());
        
        float c = dot(ray.getOrigin()-center, ray.getOrigin()-center) - (r * r);
        
        
        //t*t*dot(B, B) + 2*t*dot(B,A-C) + dot(A-C,A-C) - R*R = 0
        float s1, s2;

        if (!solve_quadratic_quation(a, b, c, &s1, &s2)) return false;
        if (s2 <  0){
            return false;
        }
        if (out_t != nullptr) {
          *out_t = s2;
        }

        vec3 intersect_position = ray.point_at_parameter(s2);
        if (out_intersect_position != nullptr) {
          *out_intersect_position = ray.point_at_parameter(s2);
        }
        if (out_intersect_normal != nullptr) {
          *out_intersect_normal = normalize(intersect_position - center);
        }

        if (out_color != nullptr) {
          *out_color = color;
        }
        return true;
    }
    
//    vec3 makeLight(Ray &ray){
//        float t = 0.0;
//        vec3 out_intersect;
//        vec3 normal;
//        vec3 color;
//        bool result_hit_sphere = this->intersect_ray(ray, &out_intersect, &normal, &t, &color);
//        if(result_hit_sphere){
//            vec3 N = normalize(ray.point_at_parameter(t) - center);
//            float half = .5f;
//            N= half * vec3(((N.x)+1 , (N.y )+1 , (N.z)+1));
//            return N;
//        }
//        vec3 unit_direction = normalize(ray.getDirection());
//        t = .5* (unit_direction.y + 1.0);
//        float x = 1.0 - t;
//        vec3 res =x * vec3(1.0,1.0,1.0);
//        vec3 res1 = t * vec3(.5,.7,1.0);
//        return res + res1;
//
//    }
};

class Triangle : public Hitable{
    private :
    vec3 node1 , node2 , node3 , color;
    public :
    Triangle(vec3 node1 , vec3 node2 ,vec3 node3 , vec3 color){
        this->node1 = node1;
        this->node2 = node2;
        this->node3 = node3;
        this->color = color;
    }
    vec3 getFirstNode(){
        return this->node1;
    }
    vec3 getSecondNode(){
        return this->node2;
    }
    vec3 getThirdNode(){
        return this->node3;
    }
    bool intersect_ray(Ray& ray, vec3* out_intersect_position, vec3* out_intersect_normal, float* out_t, vec3* out_color){
        vec3 n2n1 = node2 - node1;
        vec3 n3n1 = node3 - node1;
        vec3 crosseProductResult = cross(n2n1, n3n1);//N
        //float triangleArea = crosseProductResult.length()/2;
        float denom = dot(crosseProductResult , crosseProductResult);
        float crossProductRayDirection = dot(crosseProductResult, ray.getDirection());
        
        
        
        float d = dot(crosseProductResult, node1);
        float t = (dot(crosseProductResult, ray.getOrigin()) + d) / crossProductRayDirection;
          // check if the triangle is in behind the ray
        if (t < 0) return false;
        vec3 p = ray.point_at_parameter(t);
        vec3 C; // vector perpendicular to triangle's plane
        
        //inside_outside test
        
           // edge 1
        vec3 edge1 = node2 - node1;
        vec3 vp1 = p - node1;
        C = cross(edge1, vp1);
        if (dot(crosseProductResult, C) < 0) return false; // P is on the right side
        
           // edge 2
        vec3 edge2 = node3 - node2;
        vec3 vp2 = p - node2;
        C = cross(edge2, vp2);
        if (dot(crosseProductResult, C)  < 0)  return false; // P is on the right side
        
           // edge 3
        vec3 edge3 = node1 - node3;
        vec3 vp3 = p - node3;
        C = cross(edge3, vp3);
        if (dot(crosseProductResult, C) < 0) return false; // P is on the right side;
        if(out_intersect_position != nullptr){
            *out_intersect_position = p;
        }
        if(out_color != nullptr){
            *out_color = color;
        }
        return true;
    }
};

float calculateConsinus(vec3 normal , Ray& ray){ // cos theta = |a.b|/|a|*|b|
    float x = dot(normal,ray.getDirection());
    auto len = length(x);
    auto normalLength = length(normal);
    auto rayDirectionLength = length(ray.getDirection());
    return (len / (normalLength * rayDirectionLength));
    
}
int main() {
    string fileName = "/Users/aminkazemi/Desktop/ray.png"; // in macos we have to set relative path
    Camera cam(fileName,500,500,(float)glm::radians(90.0));
    Plane plane(vec3(1,0,0),-1,vec3(255 , 255,255));
    
    Sphere sphere(vec3(0,-2,-5), 1 , vec3(3,111,252));
    //node2 , node0 , node1
    Triangle triangle(vec3(2,1.5,-2) , vec3(1.5,1,-3) , vec3(1,0,-1) , vec3(3.0,90.0,252.0)); //y,x,z
    vector<Hitable*> v;
    v.push_back(&plane);
    v.push_back(&sphere);
    v.push_back(&triangle);
    for(int i=0; i<cam.image.getWidth();i++){
        for(int j=0; j<cam.image.getHeight();j++){
            Ray ray = cam.get_pixel_ray(i,j);
            vec3 min_color(1.0, 0.0, 1.0);
            vec3 min_position;
            vec3 min_normal;
            float min_t = 100000.0f;
            float angle;
            for(Hitable* h: v){
                vec3 color;
                vec3 position;
                vec3 normal;
                float t;
                if(h->intersect_ray(ray,&position, &normal, &t,&color)){
                    if (t <min_t){
                        min_t = t;
                        min_position = position;
                        min_color = color;
                        min_normal = normal;
                    }
                }
                angle = calculateConsinus(min_normal, ray);
                if(angle > 180) angle -=180;
            }
            cam.image.setPixel(i, j, min_color , angle);
        }
    }
        cam.image.createImageFile(cam.image.buffer);

}
