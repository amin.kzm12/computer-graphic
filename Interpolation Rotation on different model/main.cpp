#include <iostream>
#include <OpenGL/gl3.h>
#include <GLFW/glfw3.h>
#include <fstream>
#include <string>
#include <sstream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include<glm/common.hpp>
#include <glm/gtx/quaternion.hpp>
#include <vector>
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "Shader.h"
#include "Mesh.h"
#include "Model.h"
#include "Camera.h"
#include "EulerAngle.h"
#include "QuaterInterpolate.h"

#define HEIGHT 1080
#define WIDTH 1920
#define INITIALRADIAN 0.0f


void framebufferSizeCallback(GLFWwindow * window, int width, int height);
void processInput(GLFWwindow*, Camera*, float);
void renderOjects(Shader, Camera*, Model);
void renderModel(Shader, Camera*, Model, EulerAngle*,QuaterPolate*);


using namespace std;


int main() {
	glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT, "LearnOpenGL", NULL, NULL);

	if (window == NULL) {
		cout << "Failed to create GLFW window" << endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);



	glEnable(GL_DEPTH_TEST);

	Shader modelShader("/Users/aminkazemi/Desktop/graphic/HW5/HW5/model_vertexshader.vert", "/Users/aminkazemi/Desktop/graphic/HW5/HW5/model_fragmentshader.frag");
	Model model("/Users/aminkazemi/Desktop/Graphic/HW5/HW5/aminOBJ/rock.obj");

	EulerAngle e(vec3(10,15,10),vec3(280,90,280),0);

	Camera camera;

	glViewport(0, 0, WIDTH, HEIGHT);
	glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);


	modelShader.use();
	modelShader.setInt("diffuseTexture", 0);
	modelShader.setInt("shadowMap", 1);


	float deltaTime = 0.0f;
	float lastFrame = 0.0f;


	while (!glfwWindowShouldClose(window)){
		float currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;
		lastFrame = currentFrame;
		quaterpol.qtime = deltaTime;
		// input
		// -----
		processInput(window, &camera, deltaTime);

		// render
		// ------
        glClearColor(0.166f, 0.251f, 0.068f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		modelShader.use();
		renderModel(modelShader, &camera, model,&e,&quaterpol);


		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	exit(1);
	return 0;

}

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
}

//organzie input valuse 
void processInput(GLFWwindow* window, Camera* camera, float deltaTime) {
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);
	camera->processMovementInput(window, deltaTime);
}

void renderModel(Shader sh, Camera* camera, Model mModel,EulerAngle* e,QuaterPolate*q1) {
	glm::mat4 projection = glm::mat4(1.0f);
	projection = glm::perspective(glm::radians(45.0f), (float)WIDTH / (float)HEIGHT, 0.1f, 100.0f);
	sh.setMVP("projection", projection);

	glm::mat4 view = camera->getViewPosition();
	sh.setMVP("view", view);
	
	glm::mat4 model = glm::mat4(1.0f);
	model = glm::translate(model, glm::vec3(0.0, 0.0f, 0.0f)); // translate it down so it's at the center of the scene
	model = e->EulerAngleRotation(model);
	//model = q1->quaternionInterpolation(model);
	
	model = glm::scale(model, glm::vec3(0.13f, 0.13f, 0.13f));	// it's a bit too big for our scene, so scale it down
	sh.setMVP("model", model);

	mModel.Draw(sh);
}
