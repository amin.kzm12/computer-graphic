using namespace std;
using namespace glm;

class QuaterPolate {
public:
	quat q1, q2;
	float qtime = 0;
	QuaterPolate(quat a,quat b) {
		q1 = a;
		q2 = b;
	}

	mat4 quaternionInterpolation(glm::mat4& model) {
		glm::quat inverseQ1 = glm::inverse(q1);
		glm::quat interpolated = q1 * glm::pow(inverseQ1 * q2, qtime);
		glm::mat4 R = toMat4(interpolated);
		model = model * R;
		return model;
	}
};