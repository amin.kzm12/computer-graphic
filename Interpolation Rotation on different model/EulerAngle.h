
using namespace std;
using namespace glm;

class EulerAngle {
public:
	EulerAngle(vec3 x, vec3 y,int k) {
		a = x;
		b = y;
		level = k;//step
        //first time turn into a then turn into b
	};
	vec3 aroundX();
	vec3 aroundY();
	vec3 aroundZ();
	float rotateX();
	float rotateY();
	float rotateZ();
	mat4 EulerAngleRotation(mat4 model);
	vec3 a, b;
private:
	int level;
	float angleStep = 0.05f;
};

vec3 EulerAngle::aroundX() {
	return vec3(1.0f,0.0f,0.0f);
}
vec3 EulerAngle::aroundY() {
	return vec3(0.0f, 1.0f, 0.0f);
}
vec3 EulerAngle::aroundZ() {
	return vec3(0.0f, 0.0f, 1.0f);
}

float EulerAngle::rotateX() {
	if (a.x < b.x)
		a.x += angleStep; //each time add step in order to reach B

	return a.x;
}

float EulerAngle::rotateY() {
	if (a.y < b.y)
		a.y += angleStep;

	return a.y;
}

float EulerAngle::rotateZ() {
	if (a.z < b.z)
		a.z += angleStep;
	return a.z;
}

mat4 EulerAngle::EulerAngleRotation(mat4 model) {
	switch (level) {
	case 0:
		model = glm::rotate(model, glm::radians(a.x),aroundX());
		model = glm::rotate(model, glm::radians(a.y), aroundY());
		model = glm::rotate(model, glm::radians(a.z), aroundZ());
		level=1;
		return model;
	case 1:
		model= glm::rotate(model, glm::radians(rotateX()), aroundX());
		model = glm::rotate(model, glm::radians(rotateY()), aroundY());
		model = glm::rotate(model, glm::radians(rotateZ()), aroundZ());
		return model;
	default:
		return model;
	}
}

