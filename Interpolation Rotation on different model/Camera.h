#pragma once
#include<iostream>
#include <OpenGL/gl3.h>
#include <GLFW/glfw3.h>
#include <fstream>
#include <string>
#include <sstream>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>


using namespace std;
using namespace glm;

enum cameraDirection { FORWARD, BACKWARD, LEFT, RIGHT };

class Camera {
private:
	vec3 cameraPos = vec3(0.0, 0.0, 2.0);
	vec3 cameraFront = vec3(0.0, 0.0, -1.0);
	vec3 cameraUp = vec3(0.0, 1.0, 0.0);
	float cameraSpeed = 2.5f;
public:
	Camera() {
	}
	mat4 getViewPosition();
	void setCameraPos(vec3);
	void setCameraFront(vec3);
	void setCameraUp(vec3);
	vec3 getCamerPos();
	vec3 getCameraFront();
	void processMovementInput(GLFWwindow* window, float deltaTime);
	void processkeys(cameraDirection direction, float deltaTime);
};

mat4 Camera::getViewPosition() {
	return lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
}

void Camera::setCameraFront(vec3 v) {
	cameraFront = v;
}

void Camera::setCameraPos(vec3 v) {
	cameraPos = v;
}

void Camera::setCameraUp(vec3 v) {
	cameraUp = v;
}

vec3 Camera::getCamerPos() {
	return cameraPos;
}

vec3 Camera::getCameraFront() {
	return cameraFront;
}

void Camera::processMovementInput(GLFWwindow* window, float deltaTime) {
	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
		processkeys(FORWARD, deltaTime);

	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		processkeys(BACKWARD, deltaTime);

	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		processkeys(RIGHT, deltaTime);

	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		processkeys(LEFT, deltaTime);

}

void Camera::processkeys(cameraDirection direction, float deltaTime) {
	float velocity = cameraSpeed * deltaTime;
	if (direction == FORWARD)
		cameraPos += cameraFront * velocity;
	if (direction == BACKWARD)
		cameraPos -= cameraFront * velocity;
	if (direction == RIGHT)
		cameraPos += normalize(cross(cameraFront, cameraUp)) * velocity;
	if (direction == LEFT)
		cameraPos -= normalize(cross(cameraFront, cameraUp)) * velocity;

}

/*
cameraPos += cameraSpeed * cameraFront;
cameraPos -= cameraSpeed * cameraFront;
cameraPos -= normalize(cross(cameraFront, cameraUp)) * cameraSpeed;
cameraPos += normalize(cross(cameraFront, cameraUp)) * cameraSpeed;
*/
